<?php

namespace IdeaBundle\Controller;

use IdeaBundle\Entity\Idea;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Idea controller.
 *
 * @Route("idea")
 */
class IdeaController extends Controller
{
    /**
     * Lists all idea entities.
     *
     * @Route("/", name="idea_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ideas = $em->getRepository('IdeaBundle:Idea')->findAll();

        return $this->render('idea/index.html.twig', array(
            'ideas' => $ideas,
        ));
    }

    /**
     * Creates a new idea entity.
     *
     * @Route("/new", name="idea_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $idea = new Idea();
        $form = $this->createForm('IdeaBundle\Form\IdeaType', $idea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($idea);
            $em->flush();

            return $this->redirectToRoute('idea_show', array('id' => $idea->getId()));
        }

        return $this->render('idea/new.html.twig', array(
            'idea' => $idea,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a idea entity.
     *
     * @Route("/{id}", name="idea_show")
     * @Method("GET")
     */
    public function showAction(Idea $idea)
    {
        $deleteForm = $this->createDeleteForm($idea);

        return $this->render('idea/show.html.twig', array(
            'idea' => $idea,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing idea entity.
     *
     * @Route("/{id}/edit", name="idea_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Idea $idea)
    {
        $deleteForm = $this->createDeleteForm($idea);
        $editForm = $this->createForm('IdeaBundle\Form\IdeaType', $idea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('idea_edit', array('id' => $idea->getId()));
        }

        return $this->render('idea/edit.html.twig', array(
            'idea' => $idea,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a idea entity.
     *
     * @Route("/{id}", name="idea_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Idea $idea)
    {
        $form = $this->createDeleteForm($idea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($idea);
            $em->flush();
        }

        return $this->redirectToRoute('idea_index');
    }

    /**
     * Creates a form to delete a idea entity.
     *
     * @param Idea $idea The idea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Idea $idea)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('idea_delete', array('id' => $idea->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
