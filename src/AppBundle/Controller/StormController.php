<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StormController extends Controller {

//this is the route:
    /**
     * @Route("/")
     */

//This in the controller:
    public function mostStormyAction(){
        $storm = 'Welcome in the storm!';
        return $this->render('stormy/most.html.twig', array(
            'storm' => $storm)
        );
    }
} 